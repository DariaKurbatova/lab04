public class Dress{
	private double price;
	private String color;
	private String length;
	
	public Dress(double newPrice, String newColor, String newLength){
		this.price = newPrice;
		this.color = newColor;
		this.length = newLength;
	}
	
	public void discount(){
		this.price = (this.price*0.5);
	}
	public void setPrice(double newPrice){
		this.price=newPrice;
	}
	public void setColor(String newColor){
		this.color=newColor;
	}
	public void setLength(String newLength){
		this.length=newLength;
	}
	public double getPrice(){
		return this.price;
	}
	public String getColor(){
		return this.color;
	}
	public String getLength(){
		return this.length;
	}
}
